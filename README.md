# GIT tp utilisation remote - setup de base 

## Un peu de configuration 

Pour commencer vous pouvez créer un compte sur `gitlab.com` 

    🚧 ⚠️ 💀 l'utilisation de la plateforme gitlab.com n'est qu'à but pédagogique ici. Pour tout le reste de vos activités vous ne devez pas utiliser les plateformes publiques (gitlab ou github) mais vous devez impérativement utiliser le gitlab interne du Centre des Matériaux 💀 ⚠️ 🚧.

Pour faire des pull/push sur un dépot distant vous allez devoir à un moment vous authentifier. Pour cela deux solutions : 

- Login/mot de passe au moment des push/pull (fonctionne avec gitlab mais plus avec github)
- Configuration d'une clé ssh 

Pour la seconde solution vous devez procéder de la manière suivante : 

1. Ouvrir un terminal (powershell pour les windows)
2. Taper la commande ssh-keygen 
    - Valider le nom du fichier (attention à ce que ça ne vous écrae pas une clé déjà existante)
    - Saisir une passphrase 🔐
3. Récupérer le contenu du fichier `~/.ssh/id_rsa.pub` qui a été généré 
4. Sur gitlab.com aller dans [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys) et copier le contenu du fichier `id_rsa.pub`




## Objectif du TP 

### Partie 1 : tout se passe bien 

1. Créer un dépôt vide sur gitlab.com (privé ou public)
2. Cloner ce dépôt localement dans deux dossier différents que l'on appelera `machineA`et `machineB` (l'idée et de modéliser ici le fait que vous puissiez à la fois travailler sur votre portable et sur le cluster par exemple) 
3. Dans le dossier `machineA` : 
    1. Créer un fichier README.md et faire un commit 
    2. Pousser vos modification sur le gitlab
4. Dans le dossier `machineB`
    1. Tirer les mises à jour du gitlab 
    2. modifier le fichier README.md 
    3. Faire un commit et pousser sur le gitlab 
5. Dans le dossier `machineA` : 
    1. Créer un fichier License.txt et faire un commit 
    2. Essayer de pousser vos modifications 
    3. Faites ce que GIT demande 😯

### Partie 2 

1. Dans le dossier `machineA` : 
    1. Créer une branche `new-feature` à partir de `HEAD`
    2. Ajouter un fichier `truc_de_ouf.py` contenant : 
        ```python 
        print("Hello World")
        ```
    3. Faire un commit
    4. Pousser la branche sur le gitlab 
2. Dans le dossier `machineB` : 
    1. Faire une mise à jour 
    2. Basculer sur la branche `new-feature` 
    3. Changer le contenu du fichier `truc_de_ouf.py` par 
        ```python
        print("Hello World !!")
        ```
    4. Faire un commit 
3. Dans le dossier `machineA` : 
    1. Changer le contenu du fichier `truc_de_ouf.py` par 
        ```python
        print("🚀 Hello World 👋🏻")
        ```
    2. Faire un commit et pousser 
4. Dans le dossier `machineB` : 
    1. Essayer de pousser 
    2. Du coup tirer les mises à jour 💣️
    3. Résoudre le problème 
5. Faire en sorte que dans `machineA` et `machineB` l'on ait la même chose !! 

## Aide : les commandes nécessaires

```bash
git pull 
git push 
git branch 
git add 
git commit  
```